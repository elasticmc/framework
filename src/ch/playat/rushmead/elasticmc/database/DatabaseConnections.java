package ch.playat.rushmead.elasticmc.database;

import ch.playat.rushmead.elasticmc.framework.Framework;
import code.husky.mysql.MySQL;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class DatabaseConnections {

    private static MySQL mysql;
    private static Connection c;

    public static Connection getConnection() throws SQLException {
        if(c == null || c.isClosed()){
            openConnection();
        }
        return c;
    }

    public static void openConnection() {
        if (Bukkit.getIp()  == "78.129.165.154"){
            mysql = new MySQL(Framework.getInstance(), "rushmead.playat.ch", "3306", "elasticmc", "root", "");
        }else{
            mysql = new MySQL(Framework.getInstance(), "rushmead.playat.ch", "3306", "elasticmc", "rushmead", "dankmemes");
        }
        c = mysql.openConnection();
    }

    public static void closeConnection() {

        mysql.closeConnection();
    }

}
