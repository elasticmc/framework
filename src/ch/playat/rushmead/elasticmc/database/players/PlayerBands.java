package ch.playat.rushmead.elasticmc.database.players;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class PlayerBands {

    public synchronized int getElasticBands(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_elasticbands` FROM `players` WHERE `player_username`= ?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            boolean hasBands = rs.next();
            if (hasBands) {
                return rs.getInt("player_elasticbands");
            }

            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public synchronized void setBands(String username, int bands) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_elasticbands`=? WHERE `player_username`=?");
            ps.setInt(1, bands);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
