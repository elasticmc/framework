package ch.playat.rushmead.elasticmc.database.players;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Players;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class PlayerPermissions {

    public PermissionSet getSetFromPower(int power) {
        switch (power) {
            case 1:
                return PermissionSet.VIB;
            case 2:
                return PermissionSet.VIB_PLUS;
            case 3:
                return PermissionSet.MIB;
            case 4:
                return PermissionSet.MIB_PLUS;
            case 5:
                return PermissionSet.JNR_STAFF;
            case 6:
                return PermissionSet.STAFF;
            case 7:
                return PermissionSet.SNR_STAFF;
            case 8:
                return PermissionSet.ALL;
            case 0:
                return PermissionSet.DEFAULT;
        }
        return PermissionSet.DEFAULT;
    }

    public synchronized PermissionSet getPermissions(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `player_permissions` FROM `players` WHERE `player_username` =?");
            ps.setString(1, uuid);

            ResultSet results = ps.executeQuery();
            boolean isAccount = results.next();

            if (isAccount) {
                return getSetFromPower(results.getInt("player_permissions"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return PermissionSet.DEFAULT;
    }

    public synchronized void setPermissions(String username, int permissions) {
        if (!Players.getData().isInDatabase(username)) {
            Players.getData().addPlayer(username, "");
        }
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_permissions`=? WHERE `player_username`=?");
            ps.setInt(1, permissions);
            ps.setString(2, username);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
