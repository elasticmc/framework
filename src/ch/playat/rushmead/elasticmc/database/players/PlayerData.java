package ch.playat.rushmead.elasticmc.database.players;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class PlayerData {

    public synchronized void addPlayer(String name, String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `players`(`player_username`, `player_uuid`, `player_firstLogin`, `player_lastLogin`) VALUES (?,?,?,?)");
            ps.setString(1, name);
            ps.setString(2, uuid);
            ps.setString(3, "" + System.currentTimeMillis() / 1000);
            ps.setString(4, "" + System.currentTimeMillis() / 1000);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void addPlayer(Player p) {

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `players`(`player_username`, `player_uuid`, `player_lastip`,`player_firstLogin`, `player_lastLogin`) VALUES (?,?,?,?,?)");
            ps.setString(1, p.getName());
            ps.setString(2, p.getUniqueId().toString());
            ps.setString(3, p.getAddress().getHostString());
            ps.setString(4, "" + System.currentTimeMillis() / 1000);
            ps.setString(5, "" + System.currentTimeMillis() / 1000);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public synchronized boolean isInDatabase(String name) {
        try {

            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username` = ?");
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            boolean inDatabase = rs.next();
            return inDatabase;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized ElasticPlayer getPlayerFromName(Player p) {
        ElasticPlayer player = null;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_username`=?");
            ps.setString(1, p.getDisplayName());

            ResultSet results = ps.executeQuery();
            boolean isUser = results.next();
            if (isUser) {
                String uuid = results.getString("player_uuid");
                if (uuid == null || uuid.length() == 0) {
                    Players.getData().updateUUIDFromName(p);
                }
                player = new ElasticPlayer(p.getDisplayName(), p.getUniqueId().toString(), results.getInt("player_rank"), results.getInt("player_permissions"));
                return player;

            } else {
                throw new NullPointerException();
            }
        } catch (SQLException e) {
            e.printStackTrace();

        } catch (NullPointerException ex) {
            this.addPlayer(p);
        }
        return player;
    }

    public synchronized ElasticPlayer getPlayerFromUUID(Player p) {
        ElasticPlayer player = null;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `players` WHERE `player_uuid`=?");
            ps.setString(1, p.getUniqueId().toString());

            ResultSet results = ps.executeQuery();
            boolean isUser = results.next();
            if (isUser) {
                player = new ElasticPlayer(p.getDisplayName(), p.getUniqueId().toString(), results.getInt("player_rank"), results.getInt("player_permissions"));
                return player;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }

    public synchronized void updateNameFromUUID(Player p) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_username`=? WHERE `player_uuid` = ?");
            ps.setString(1, p.getDisplayName());
            ps.setString(2, p.getUniqueId().toString());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateUUIDFromName(Player p) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid`=? WHERE `player_username` = ?");
            ps.setString(1, p.getUniqueId().toString());
            ps.setString(2, p.getDisplayName());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void savePlayerToDB(Player p) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `players` SET `player_uuid` = ? WHERE `player_username` = ?");

            ps.setString(1, p.getUniqueId().toString());
            ps.setString(2, p.getDisplayName());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
