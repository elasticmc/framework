package ch.playat.rushmead.elasticmc.database.players;

/**
 *
 * @author Rushmead
 */
public enum Achievement {

    halfDay("Half Day", "Play on ElasticMC for 12 hours.", 100),
    winFirstGame("1st Win", "Win your first game.", 100),
    notAlone("Im not alone!", "Play a game with a friend.", 100),
    thousand("1k Bands", "Accumulate 1k Elastic Bands.", 100),
    oneDay("One Day", "Play on ElasticMC for 24 hours.", 200),
    friendly("Friendly", "Have 10 players on your friends list.", 200),
    partyUp("Party Up", "Have 6 players in a party at one given time.", 200),
    doubleBands("Double Bands", "Get double Elastic Bands", 200),
    tenThousand("10k Bands", "Accumulate 10k Elastic Bands.", 200),
    oneWeek("One Week", "Play on ElasticMC for a week.", 500),
    freshSpawn("Fresh Spawn", "Welcome to ElasticMC.", 500),
    secretWord("Secret Word", "Say the secret word of the month in chat.", 500),
    whatsThat("Whats that?", "Is it a bird? Is it a plane? No, its ButtHunter.", 500), // Everyone who is in Cloudys friend or party gets this!
    hundredThousand("100k Bands", "Accumulate 100k Elastic Bands.", 500),
    soTotallyParty("SoTotallyParty", "Somehow get into a party with SoTotallyXray.", 500),
    oneMonth("One whole month!", "Play on ElasticMC for a month", 1000),
    premiumElastic("Premium", "Become a premium member of ElasticMC, Thank you for supporting us! <3.", 1000),
    rushNoob("Rushn00b", "Somehow get into a party with Rushmead.", 1000),
    oneYear("Time Waster", "Play on ElasticMC for a year.", 10000),
    hundredFriends("ONE HUNDRED FRIENDS", "Have 100 friends on your friends list, then go to jail.", 10000);

    private String name;
    private String description;
    private int elasticBands;

    Achievement(String name, String description) {
        this.name = name;
        this.description = description;
    }

    Achievement(String name, String description, int elasticBands) {
        this.name = name;
        this.description = description;
        this.elasticBands = elasticBands;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getElasticBands() {
        return elasticBands;
    }

}
