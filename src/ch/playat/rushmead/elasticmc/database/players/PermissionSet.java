package ch.playat.rushmead.elasticmc.database.players;

/**
 *
 * @author Rushmead
 */
public enum PermissionSet {

    ALL(8),
    SNR_STAFF(7),
    STAFF(6),
    JNR_STAFF(5),
    MIB_PLUS(4),
    MIB(3),
    VIB_PLUS(2),
    VIB(1),
    DEFAULT(0);
    private int power;

    PermissionSet(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

}
