package ch.playat.rushmead.elasticmc.database.players;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Sound;

/**
 *
 * @author Rushmead
 */
public class PlayerAchievements {

    public synchronized void addPlayerToDatabase(String uuid) {
        try {

            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO `achievements`(`id`, `uuid`) VALUES (?, ?)");
            ps.setInt(1, 0);
            ps.setString(2, uuid);
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void displayAchievement(Player p, Achievement a) {
        ElasticPlayer.getPlayer(p.getDisplayName()).addAchievement(a);
        Messaging.sendBlankMessage(p);

        Messaging.sendRawMessage(p, StringUtils.center("&e\u2726 &6&lACHIEVEMENT UNLOCKED &e\u2726", 69));
        Messaging.sendRawHoveringMessage(p, StringUtils.center(ChatColor.AQUA + a.getName(), 83), a.getDescription());

        p.playSound(p.getLocation(), Sound.NOTE_PLING, 1f, 1f);
    }

    public boolean hasAchievement(String uuid, Achievement a) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM achievements WHERE uuid=?");
            ps.setString(1, uuid);
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                if (result.getBoolean(a.getName().toLowerCase())) {
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean giveAchievement(Player p, Achievement a) {
        try {
            ElasticPlayer ap = ElasticPlayer.getPlayer(p.getDisplayName());

            if (ap.getAchievements() == null) {
                if (isInDatabase(p.getUniqueId().toString())) {
                    displayAchievement(p, a);
                    return true;
                }
            } else {
                if (!ap.getAchievements().contains(a)) {
                    displayAchievement(p, a);
                    return true;
                }
            }
        } catch (NullPointerException ex) {
        }
        return false;
    }

    public synchronized boolean isInDatabase(String uuid) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `uuid` FROM `achievements` WHERE `uuid` = ?");
            ps.setString(1, uuid);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public synchronized ArrayList<Achievement> getAchievements(String uuid) {
        ArrayList<Achievement> achievements = new ArrayList<>();
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `achievements` WHERE `uuid` = ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                for (Achievement achievement : Achievement.values()) {
                    if (rs.getBoolean(achievement.name())) {
                        achievements.add(achievement);
                    }
                }
            } else {
                throw new NullPointerException();
            }
        } catch (SQLException ex) {
        } catch (NullPointerException ex) {
            if (!isInDatabase(uuid)) {
                addPlayerToDatabase(uuid);
            }

        }
        return achievements;
    }

    public synchronized void updateAchievements(Player p) {

        try {
            Connection c = DatabaseConnections.getConnection();

            String list = "";
            for (Achievement a : Achievement.values()) {
                list += "`" + a.name() + "`=?, ";
            }
            list = list.substring(0, list.length() - 2);
            PreparedStatement ps = c.prepareStatement("UPDATE `achievements` SET " + list + " WHERE `uuid`=?");
            ArrayList<Achievement> playerAchievements = ElasticPlayer.getPlayer(p.getDisplayName()).getAchievements();

            for (int i = 0; i < Achievement.values().length; i++) {
                ps.setBoolean(i + 1, playerAchievements.contains(Achievement.values()[i]));
            }
            ps.setString(Achievement.values().length + 1, p.getUniqueId().toString());

            ps.executeUpdate();
        } catch (NullPointerException ex) {

        } catch (SQLException ex) {
        }
    }

}
