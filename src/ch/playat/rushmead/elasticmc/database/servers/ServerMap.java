package ch.playat.rushmead.elasticmc.database.servers;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class ServerMap {

    public synchronized String getMapName(String name) {
        name = name.toLowerCase();
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("SELECT `mapname` FROM `servers` WHERE `nickname`=?");
            s.setString(1, name);

            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                return rs.getString("mapname");
            } else {
                rs.close();
                s.close();
            }
        } catch (SQLException ex) {

        }
        return "Unknown Map";
    }

    public synchronized void setMapName(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `servers` SET `mapname`=? WHERE `nickname`=?");
            ps.setString(1, name);
            ps.setString(2, Servers.getData().getName());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException ex) {
        }
    }
}
