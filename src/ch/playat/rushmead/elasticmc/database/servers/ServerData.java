package ch.playat.rushmead.elasticmc.database.servers;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.Bukkit;

/**
 *
 * @author Rushmead
 */
public class ServerData {

    private String serverName;

    public synchronized boolean isInDatabase() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * FROM `servers` WHERE `nickname` = ?");
            ps.setString(1, serverName);
            ResultSet res = ps.executeQuery();
            if (res.next()) {
                res.close();
                ps.close();
                return true;
            }

        } catch (Exception e) {

        }
        return false;
    }

    public String getName() {
        if (serverName != null) {
            return serverName;
        } else {
            String name = getNameFromDB();
            setName(name);
            return name;
        }
    }

    public synchronized String getNameFromDB() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `nickname` FROM `servers` WHERE `ip`=? AND `port`=?");
            ps.setString(1, "78.129.165.154");
            ps.setInt(2, Bukkit.getPort());
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getString("nickname");
            }
        } catch (SQLException e) {

        }
        return "Unkown";
    }

    public void setName(String name) {
        this.serverName = name;
    }

    public synchronized boolean isInactive(String server) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `lastchange` FROM `servers` WHERE `nickname`=?");
            ps.setString(1, server);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return Integer.parseInt(rs.getString("lastchange")) < (System.currentTimeMillis() / 1000) - 60;
            } else {
                rs.close();
                ps.close();
            }
        } catch (SQLException e) {

        } catch (NumberFormatException ex) {

        }
        return true;
    }

    public synchronized void updateAllInfo() {

        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE `servers` SET `gamestate`=?, `mapname`=?, `players`=?, `lastchange`=? WHERE `nickname`=?");
            ps.setInt(1, Servers.getState().getNumberFromState(GameState.getGameState()));
            ps.setString(2, Servers.getMap().getMapName(serverName));
            ps.setInt(3, Bukkit.getOnlinePlayers().size());
            ps.setString(4, "" + System.currentTimeMillis() / 1000);
            ps.setString(5, serverName);
            ps.executeUpdate();
        } catch (SQLException e) {

        }
    }
}
