package ch.playat.rushmead.elasticmc.database.servers;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class ServerWhitelist {

    private boolean whitelisted;

    public boolean isWhitelisted() {
        return whitelisted;
    }

    public synchronized boolean isWhitelistedFromDB() {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("SELECT `whitelisted` FROM `servers` WHERE `nickname`=?");
            s.setString(1, Servers.getData().getName());

            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                this.whitelisted = rs.getBoolean("whitelisted");
                return rs.getBoolean("whitelisted");
            } else {
                rs.close();
                s.close();
            }
        } catch (SQLException ex) {

        }
        this.whitelisted = false;
        return false;
    }

    public synchronized boolean isWhitelistedFromDB(String name) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("SELECT `whitelisted` FROM `servers` WHERE `nickname`=?");
            s.setString(1, name);

            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("whitelisted");
            } else {
                rs.close();
                s.close();
            }
        } catch (SQLException ex) {

        }
        this.whitelisted = false;
        return false;
    }

    public synchronized void setWhitelisted(boolean whitelisted) {
        this.whitelisted = whitelisted;
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("UPDATE `servers` SET `whitelisted`=? WHERE `nickname`=?");
            s.setBoolean(1, whitelisted);
            s.setString(2, Servers.getData().getName());
            s.executeUpdate();
        } catch (SQLException ex) {
        }
    }
}
