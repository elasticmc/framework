package ch.playat.rushmead.elasticmc.database.servers;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class ServerPlayers {

    public int getMaxPlayers(String name) {
        name = name.toLowerCase();
        if (name.startsWith("hub")) {
            return 100;
        }
        if (name.startsWith("laser")) {
            return 12;
        }
        return 100;
    }

    public synchronized int getPlayers(String name) {
        name = name.toLowerCase();
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("SELECT `players` FROM `servers` WHERE `nickname`=?");
            s.setString(1, name);

            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                return rs.getInt("players");
            } else {
                rs.close();
                s.close();
            }
        } catch (SQLException ex) {
        }
        return 0;
    }

    public synchronized void setPlayers(int players) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("UPDATE `servers` SET `players`=? WHERE `nickname`=?");
            s.setInt(1, players);
            s.setString(2, Servers.getData().getName());
            s.executeUpdate();
        } catch (SQLException ex) {

        }
    }

}
