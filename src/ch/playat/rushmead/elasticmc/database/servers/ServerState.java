package ch.playat.rushmead.elasticmc.database.servers;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rushmead
 */
public class ServerState {

    public GameState getStateFromNumber(int state) {
        if (state == 0) {
            return GameState.LOBBY;
        } else if (state == 1) {
            return GameState.SETUP;

        } else if (state >= 2 && state < 9) {
            return GameState.IN_GAME;
        } else if (state == 9) {
            return GameState.ENDING;
        }

        return GameState.NOT_IN_GAME;
    }

    public int getNumberFromState(GameState state) {
        if (state == GameState.LOBBY) {
            return 0;
        } else if (state == GameState.SETUP) {
            return 1;

        } else if (state == GameState.IN_GAME) {
            return 2;
        } else if (state == GameState.ENDING) {
            return 9;
        }
        return 10;
    }

    public synchronized int getGameState(String name) {
        name = name.toLowerCase();
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT `gamestate` FROM `servers` WHERE `nickname`=?");
            ps.setString(1, name);

            ResultSet rse = ps.executeQuery();
            if (rse.next()) {
                return rse.getInt("gamestate");

            } else {
                rse.close();
                ps.close();
            }
        } catch (SQLException e) {

        }
        return 10;
    }

    public synchronized void setGameState(int state) {
        try {
            Connection c = DatabaseConnections.getConnection();
            PreparedStatement s = c.prepareStatement("UPDATE `servers` SET `gamestate`=?, `lastchange`=? WHERE `nickname`=?");
            s.setInt(1, state);
            s.setString(2, "" + System.currentTimeMillis() / 1000);
            s.setString(3, Servers.getData().getName());
            s.executeUpdate();
        } catch (SQLException ex) {
        }
    }
}
