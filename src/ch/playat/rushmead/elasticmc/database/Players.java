package ch.playat.rushmead.elasticmc.database;

import ch.playat.rushmead.elasticmc.database.players.PlayerAchievements;
import ch.playat.rushmead.elasticmc.database.players.PlayerBands;
import ch.playat.rushmead.elasticmc.database.players.PlayerData;
import ch.playat.rushmead.elasticmc.database.players.PlayerPermissions;
import ch.playat.rushmead.elasticmc.database.players.Ranks;

/**
 *
 * @author Rushmead
 */
public class Players {

    private static PlayerAchievements pa = new PlayerAchievements();
    private static PlayerData pd = new PlayerData();
    private static PlayerPermissions pp = new PlayerPermissions();
    private static Ranks pr = new Ranks();
    private static PlayerBands pb = new PlayerBands();

    public static PlayerAchievements getAchievements() {
        return pa;
    }

    public static PlayerData getData() {
        return pd;
    }

    public static PlayerPermissions getPermissions() {
        return pp;
    }

    public static Ranks getRanks() {
        return pr;
    }

    public static PlayerBands getBands() {
        return pb;
    }
}
