package ch.playat.rushmead.elasticmc.database.logs;

/**
 *
 * @author Rushmead
 */
public enum Level {

    DEBUG("DBUG"),
    INFO("INFO"),
    WARNING("WARN"),
    ERROR("EROR"),
    CRITICAL("CRIT");

    private String tag;

    Level(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

}
