package ch.playat.rushmead.elasticmc.database;

import ch.playat.rushmead.elasticmc.database.servers.ServerData;
import ch.playat.rushmead.elasticmc.database.servers.ServerMap;
import ch.playat.rushmead.elasticmc.database.servers.ServerPlayers;
import ch.playat.rushmead.elasticmc.database.servers.ServerState;
import ch.playat.rushmead.elasticmc.database.servers.ServerWhitelist;

/**
 *
 * @author Rushmead
 */
public class Servers {

    private static ServerData sd = new ServerData();
    private static ServerState ss = new ServerState();
    private static ServerMap sm = new ServerMap();
    private static ServerPlayers sp = new ServerPlayers();
    private static ServerWhitelist sw = new ServerWhitelist();

    public static ServerData getData() {
        return sd;
    }

    public static ServerState getState() {
        return ss;
    }

    public static ServerMap getMap() {
        return sm;
    }

    public static ServerPlayers getPlayers() {
        return sp;
    }

    public static ServerWhitelist getWhitelist() {
        return sw;
    }
}
