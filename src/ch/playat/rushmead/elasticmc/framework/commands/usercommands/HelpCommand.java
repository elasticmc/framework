

package ch.playat.rushmead.elasticmc.framework.commands.usercommands;


import ch.playat.rushmead.elasticmc.framework.customevents.HelpCommandEvent;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.util.Collections;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
/**
 *
 * @author Rushmead
 */
public class HelpCommand implements CommandExecutor{


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        HelpCommandEvent e = new HelpCommandEvent();
        Bukkit.getServer().getPluginManager().callEvent(e);
        
        e.addCommand("hub");
        e.addCommand("list");
        e.addCommand("listranks");
        e.addCommand("vote <map number>");
        e.addCommand("whereami");
        
        Collections.sort(e.getCommands());
        Messaging.sendMessage(sender, "&aHelp Page &b-&7 1/1");
        for(String command : e.getCommands()){
            Messaging.sendRawMessage(sender, "&8- &6/{0}", command.toLowerCase());
        }
        return false;
    }
}
