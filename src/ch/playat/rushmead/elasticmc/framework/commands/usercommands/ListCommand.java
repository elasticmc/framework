

package ch.playat.rushmead.elasticmc.framework.commands.usercommands;


import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
/**
 *
 * @author Rushmead
 */
public class ListCommand implements CommandExecutor{


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        Messaging.sendMessage(sender, "There are &e{0} &7players online", Bukkit.getOnlinePlayers().size());
        
        for(Rank rank : Rank.values()){
            
            ArrayList<String> players = new ArrayList<>();
            for(ElasticPlayer eps : ElasticPlayer.getPlayers().values()){
                if(eps.getRank() == rank){
                    players.add(eps.getNickName());
                }
            }
            if(players.size() > 0){
                String playerList = Arrays.asList(players).toString().substring(2).replaceAll("]", " ");
                if(rank.hasTwoColours()){
                      Messaging.sendMessage(sender, "{0}{1}{2}s&8: &7{3}", rank.getColor2(), rank.getColor(), rank.getName(), playerList);
                }else{
                     Messaging.sendMessage(sender, "{0}{1}s&8: &7{2}", rank.getColor(), rank.getName(), playerList);
                }
               
            }
        }
        return false;
    }
}
