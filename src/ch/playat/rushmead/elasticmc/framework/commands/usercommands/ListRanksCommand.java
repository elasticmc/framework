

package ch.playat.rushmead.elasticmc.framework.commands.usercommands;


import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
/**
 *
 * @author Rushmead
 */
public class ListRanksCommand implements CommandExecutor{


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        String bullet = "\u2022";
        String bullets = ChatColor.DARK_GRAY + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet;
       sender.sendMessage(bullets);
       for(Rank rank : Rank.values()){
            if (rank.hasTwoColours()) {
                    Messaging.sendRawMessage(sender, rank.getColor2() + "" + rank.getColor() + rank.getName());
                } else {

                    Messaging.sendRawMessage(sender, rank.getColor() + rank.getName());
                }
       }
       sender.sendMessage(bullets);
        return false;
    }
}
