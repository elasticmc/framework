package ch.playat.rushmead.elasticmc.framework.commands.modcommands;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.logs.Log;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class KickCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && Players.getPermissions().getPermissions(((Player) sender).getDisplayName()).getPower() < PermissionSet.JNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou do not have permission to use this command");
            return false;
        }
        if (args.length < 2) {
            Messaging.sendMessage(sender, "&cIncorrect usage: /kick <username> <reason>");
            return false;
        }

        try {
            Player target = Bukkit.getPlayer(args[0]);
            if (target != null) {
                if (sender instanceof Player && Players.getPermissions().getPermissions(target.getDisplayName()).getPower() >= Players.getPermissions().getPermissions(((Player) sender).getDisplayName()).getPower()) {
                    Messaging.sendMessage(sender, "&cYou do not have permission to kick that player.");
                    return false;
                }
            } else {
                Messaging.sendMessage(sender, "&cA player with that username does not exist.");
                return false;
            }
            String kickReason = "";
            for (int i = 1; i < args.length; i++) {
                kickReason += args[i] + " ";
            }

            target.kickPlayer(Messaging.colorizeMessage(Messaging.getPrefix() + "&cYou were kicked for " + kickReason));
            Log.ogKick(sender.getName(), target.getDisplayName(), kickReason);

            for (Player ps : Bukkit.getOnlinePlayers()) {
                if (Players.getPermissions().getPermissions(ps.getName()).getPower() >= PermissionSet.JNR_STAFF.getPower()) {
                    Messaging.sendRawMessage(ps, "{3} &c{0} kicked {1} for {2}", sender.getName(), target.getDisplayName(), kickReason, Messaging.getPrefix("&5Staff"));
                }
            }
        } catch (NullPointerException e) {
            Messaging.sendMessage(sender, "&cThat player is offline.");
            return false;
        }
        return false;
    }
}
