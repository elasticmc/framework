

package ch.playat.rushmead.elasticmc.framework.commands.admincommands;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Rushmead
 */
public class AddBandsCommand implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(Players.getPermissions().getPermissions(sender.getName()).getPower() < PermissionSet.SNR_STAFF.getPower()){
            Messaging.sendMessage(sender, "&cYou can not run this command.");
            return false;
        }
        if(args.length < 2){
            Messaging.sendMessage(sender, "&cIncorrect Usage: /addbands <username> <tokens>");
            return false;
        }
        
        try{
            String name = args[0];
            if(!Players.getData().isInDatabase(name)){
                throw new NullPointerException();
            }
            int current = Players.getBands().getElasticBands(name);
            Players.getBands().setBands(name, current + Integer.parseInt(args[1]));
            Messaging.sendMessage(sender, "&6{0} now has {1} bands.", name, (current+ Integer.parseInt(args[1])));
            return true;
        }catch(NullPointerException ex){
            Messaging.sendMessage(sender, "&cCould not find that player in our database.");
            return false;
        }catch(NumberFormatException ex){
            Messaging.sendMessage(sender, "&cThat number cannot be parsed.");
            return false;
        }
    }
    
}
