

package ch.playat.rushmead.elasticmc.framework.commands.admincommands;


import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
/**
 *
 * @author Rushmead
 */
public class GMCommand implements CommandExecutor{


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        Player p = null;
        
        if(sender instanceof Player){
            p = (Player) sender;
        }
        if(!(sender instanceof Player) && args.length != 2){
            Messaging.sendMessage(sender, "&cThis is a player only command.");
        return false;
        }
        if(Players.getPermissions().getPermissions(p.getDisplayName()).getPower() < PermissionSet.STAFF.getPower()){
            Messaging.sendMessage(sender, "&cYou do not have permission to run this command.!");
            return false;
        }
        if(args.length == 0 && sender instanceof Player){
            if(p.getGameMode() != GameMode.CREATIVE){
              p.setGameMode(GameMode.CREATIVE);
              Messaging.sendMessage(sender, "&6Your gamemode was changed");
              return true;
            }else{
                p.setGameMode(GameMode.ADVENTURE);
                 Messaging.sendMessage(sender, "&6Your gamemode was changed");
                 return true;
            }
        }else if(args.length == 1){
            Player target = Bukkit.getPlayer(args[0]);
              if(target.getGameMode() != GameMode.CREATIVE){
              target.setGameMode(GameMode.CREATIVE);
              Messaging.sendMessage(target, "&6Your gamemode was changed");
              return true;
            }else{
                target.setGameMode(GameMode.ADVENTURE);
                 Messaging.sendMessage(target, "&6Your gamemode was changed");
                 return true;
            }
        }
        return false;
    }
}
