package ch.playat.rushmead.elasticmc.framework.commands.admincommands;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class SetRankCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        PermissionSet permissions = null;
        if (sender instanceof Player) {
            permissions = Players.getPermissions().getPermissions(((Player) sender).getName());
            if (permissions.getPower() < PermissionSet.SNR_STAFF.getPower()) {
                Messaging.sendMessage(sender, "&cYou do not have permission to run this command");
                return false;
            }
        } else if (sender instanceof ConsoleCommandSender) {
            permissions = PermissionSet.ALL;
        }

        if (args.length < 2 || args[1] == null) {
            Messaging.sendMessage(sender, "&cIncorrect command usageL /setrank <username> <rank>");
            Messaging.sendMessage(sender, "&3Current available ranks");
            for (Rank r : Rank.values()) {
                if (r.hasTwoColours()) {
                    Messaging.sendRawMessage(sender, r.getColor2() + "" + r.getColor() + r.getName());
                } else {

                    Messaging.sendRawMessage(sender, r.getColor() + r.getName());
                }
            }
            return false;
        }
        String rankStr = args[1];
        Rank newRank = null;

        switch (rankStr.toLowerCase()) {
            case "regular":
                newRank = Rank.REGULAR;
                break;
            case "vib":
                newRank = Rank.VIB;
                break;
            case "vib+":
                newRank = Rank.VIB_PLUS;
                break;
            case "mib":
                newRank = Rank.MIB;
                break;
            case "mib+":
                newRank = Rank.MIB_PLUS;
                break;
            case "creeperhost-staff":
                newRank = Rank.CREEPERHOSTSTAFF;
                break;
            case "helper":
                newRank = Rank.HELPER;
                break;
            case "moderator":
                newRank = Rank.MODERATOR;
                break;
            case "admin":
                newRank = Rank.ADMIN;
                break;
            case "head-admin":
                newRank = Rank.HEAD_ADMIN;
                break;
            case "owner":
                newRank = Rank.OWNER;
        }
        if (newRank == null) {
            Messaging.sendMessage(sender, "&cIncorrect command usageL /setrank <username> <rank>");
            Messaging.sendMessage(sender, "&3Current available ranks");
            for (Rank r : Rank.values()) {
                if (r.hasTwoColours()) {
                    Messaging.sendRawMessage(sender, r.getColor2() + "" + r.getColor() + r.getName());
                } else {

                    Messaging.sendRawMessage(sender, r.getColor() + r.getName());
                }
            }
            return false;
        }
        if (permissions != null && newRank.getPermissionSet().getPower() > permissions.getPower()) {
            Messaging.sendMessage(sender, "&cYou do not have permissions to set a player's permissions higher than yourself.");
            return false;
        }

        Messaging.sendMessage(sender, "&3You set {0}'s rank to {1}", args[0].toLowerCase(), args[1].toLowerCase());

        try {
            Player p = Bukkit.getPlayer(args[0]);
            ElasticPlayer ep = ElasticPlayer.getPlayer(p.getDisplayName());

            ep.setRank(newRank);
            ep.setPermissions(newRank.getPermissionSet());
        } catch (NullPointerException e) {

        }
        Players.getRanks().setRank(args[0], newRank.getID());
        Players.getPermissions().setPermissions(args[0], newRank.getPermissionSet().getPower());
        return false;
    }
}
