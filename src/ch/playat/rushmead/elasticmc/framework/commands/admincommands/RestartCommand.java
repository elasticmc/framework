package ch.playat.rushmead.elasticmc.framework.commands.admincommands;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.api.ServerSending;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class RestartCommand implements CommandExecutor {

    public static boolean restarting = false;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && Players.getPermissions().getPermissions(((Player) sender).getDisplayName()).getPower() < PermissionSet.SNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "&cYou do not have permission to run this command");
            return false;
        }
        if (restarting) {
            Messaging.sendMessage(sender, "&cServer is already restarting.");
            return false;
        }
        Bukkit.getScheduler().cancelAllTasks();
        GameState.setGameState(GameState.NOT_IN_GAME);
        int restartTime;

        try {
            restartTime = Integer.parseInt(args[0]);
        } catch (NumberFormatException | NullPointerException | ArrayIndexOutOfBoundsException ex) {
            restartTime = 5;
        }
        restarting = true;

        for (int i = restartTime; i >= -1; i--) {
            final int i2 = i;
            Bukkit.getScheduler().scheduleSyncDelayedTask(Framework.getInstance(), new Runnable() {
                @Override
                public void run() {
                    GameState.gameTimer = i2;
                    if (i2 == 0) {
                        if (Bukkit.getOnlinePlayers().size() > 0) {
                            ServerSending.sendToServer("hub1");
                        }
                    }
                    if (i2 == -1) {
                        Messaging.broadcastMessage("&4Server is restarting....");
                        Bukkit.shutdown();
                    } else if (i2 > 0) {
                        Messaging.countdown(Servers.getData().getName() + " will restart", false, ChatColor.RED, ChatColor.YELLOW);
                    }

                }

            }, (restartTime - i) * 20L);
        }
        return false;
    }
}
