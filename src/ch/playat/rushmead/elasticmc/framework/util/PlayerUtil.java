package ch.playat.rushmead.elasticmc.framework.util;

import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class PlayerUtil {

    public static void forceRespawnPlayer(Player p) {
        if (!p.isDead()) {
            return;
        }
        try {
            Object nmsPlayer = p.getClass().getMethod("getHandle").invoke(p);
            Object packet = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".PacketPlayInClientCommand").newInstance();
            Class<?> enumClass = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".EnumClientCommand");
            for (Object ob : enumClass.getEnumConstants()) {
                if (ob.toString().equals("PERFORM_RESPAWN")) {
                    packet = packet.getClass().getConstructor(enumClass).newInstance(ob);
                }
            }
            Object con = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
            con.getClass().getMethod("a", packet.getClass()).invoke(con, packet);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassNotFoundException | InstantiationException | NoSuchFieldException t) {
        }
    }

    public static void sendTitleOrSubtitle(Player player, EnumTitleAction titleType, String text, int fadeInTime, int showTime, int fadeOutTime, ChatColor color) {
        IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + Messaging.colorizeMessage(text) + "\",color:" + color.name().toLowerCase() + "}");

        if (titleType != EnumTitleAction.TITLE && titleType != EnumTitleAction.SUBTITLE) {
            return;
        }
        PacketPlayOutTitle title = new PacketPlayOutTitle(titleType, chatTitle);
        PacketPlayOutTitle length = new PacketPlayOutTitle(fadeInTime, showTime, fadeOutTime);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(length);
    }

    public static void changeNameTag(Player p, String name) {

        EntityPlayer changingName = ((CraftPlayer) p).getHandle();

        try {
            Field nameField = EntityHuman.class.getDeclaredField("bH");
            ProfileLoader loader = new ProfileLoader(p.getUniqueId().toString(), name, p.getDisplayName());

            nameField.setAccessible(true);
            nameField.set(changingName, loader.loadProfile());
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Player players : Bukkit.getOnlinePlayers()) {
            PacketPlayOutEntityDestroy pac = new PacketPlayOutEntityDestroy(changingName.getId());
            ((CraftPlayer) players).getHandle().playerConnection.sendPacket(pac);

            if (players != p) {
               
            }
        }
    }
}
