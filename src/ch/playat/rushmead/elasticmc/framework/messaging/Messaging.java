package ch.playat.rushmead.elasticmc.framework.messaging;

import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class Messaging {

    private static String _prefix = colorizeMessage("&3Elastic&5&lMC&f | ");

    public static String getPrefix() {
        return _prefix;
    }

    public static void setPrefix(String newPrefix) {
        _prefix = newPrefix;
    }

    public static String colorizeMessage(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static void broadcastMessage(String message, Object... replacements) {
        Bukkit.broadcastMessage(getPrefix() + replace(message, replacements));
    }

    public static void sendMessage(Player p, String message, Object... replacements) {
        p.sendMessage(getPrefix() + replace(message, replacements));

    }

    public static void sendMessage(CommandSender sender, String message, Object... replacements) {
        sender.sendMessage(getPrefix() + replace(message, replacements));
    }

    public static void broadcastRawMessage(String message, Object... replacements) {
        Bukkit.broadcastMessage(replace(message, replacements));
    }

    public static void sendRawMessage(Player p, String message, Object... replacements) {
        p.sendMessage(replace(message, replacements));
    }

    public static void sendRawMessage(CommandSender sender, String message, Object... replacements) {
        sender.sendMessage(replace(message, replacements));
    }

    public static String getPrefix(String name) {
        return colorizeMessage("&a" + name + " &8\u00BB &7");
    }

    public static String replace(String msg, Object... replacements) {
        if (replacements.length > 0) {

            for (int i = 0; i < replacements.length; i++) {
                if (msg.contains("{" + i + "}")) {
                    msg = msg.replace("{" + i + "}", replacements[i].toString());
                } else {
                    msg = msg.replace("{" + i + "}", "");
                }
            }
        }
        return colorizeMessage(msg);
    }

    public static void sendBlankMessage(Player p) {
        p.sendMessage("");
    }

    public static void sendHoveringMessage(Player player, String message, String hover) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + getPrefix() + colorizeMessage(message) + "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"" + hover + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendClickableMessage(Player player, String message, String command) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + getPrefix() + colorizeMessage(message) + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendClickableAndHoveringMessage(Player player, String message, String hover, String command) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + getPrefix() + colorizeMessage(message) + "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"" + hover + "\"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendRawHoveringMessage(Player player, String message, String hover) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + colorizeMessage(message) + "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"" + hover + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendRawClickableMessage(Player player, String message, String command) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + colorizeMessage(message) + "\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void sendRawClickableAndHoveringMessage(Player player, String message, String hover, String command) {
        IChatBaseComponent comp = ChatSerializer.a("{\"text\":\"" + colorizeMessage(message) + "\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"" + hover + "\"},\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + command + "\"}}");
        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    public static void countdown(String action, boolean fast, ChatColor... colors) {
        ChatColor color1 = ChatColor.GOLD;
        ChatColor color2 = ChatColor.AQUA;
        try {
            if (colors != null && colors.length == 2) {
                color1 = colors[0];
                color2 = colors[1];
            }
        } catch (NullPointerException e) {
        }
        if (GameState.gameTimer < 0) {
            return;
        }
        if (GameState.gameTimer == 0) {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                ps.playSound(ps.getLocation(), Sound.NOTE_PLING, 1, 0);
            }
            return;
        }
        if (GameState.gameTimer == 1 && fast) {
            Messaging.broadcastMessage(color1 + action + " in " + color2 + GameState.gameTimer + color1 + " second!");
        } else if (GameState.gameTimer % 10 == 0 || GameState.gameTimer == 5 || fast && GameState.gameTimer < 5 && GameState.gameTimer > 1) {
            Messaging.broadcastMessage(color1 + action + " in " + color2 + GameState.gameTimer + color1 + " seconds!");
        }
        if (GameState.gameTimer <= 10 && fast) {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                ps.playSound(ps.getLocation(), Sound.NOTE_PLING, 1, 0);
            }
        }
    }

    public static void logmessage(String name, ChatColor color, boolean log) {
        name = Messaging.colorizeMessage("\u00bb " + color + name);
        ChatColor joinColor;

        if (log == true) {
            joinColor = ChatColor.GREEN;
        } else {
            joinColor = ChatColor.RED;
        }

        if (GameState.getGameState() != GameState.NOT_IN_GAME) {
            Messaging.broadcastRawMessage(joinColor + name);
            return;
        }
        if (color == Rank.VIB.getColor()
                || color == Rank.ADMIN.getColor()
                || color == Rank.OWNER.getColor()) {
            Messaging.broadcastRawMessage(joinColor + name);
        }
    }
}
