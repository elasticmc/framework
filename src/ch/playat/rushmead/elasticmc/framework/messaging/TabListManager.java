package ch.playat.rushmead.elasticmc.framework.messaging;

import java.lang.reflect.Field;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class TabListManager {

    private static Field bField;

    public static void updateHeaderAndFooter(String header, String footer) {
        for (Player ps : Bukkit.getOnlinePlayers()) {
            updateHeaderAndFooter(ps, header, footer);
        }
    }
    
    public static PacketPlayOutPlayerListHeaderFooter getHeaderFooterPacket(IChatBaseComponent header, IChatBaseComponent footer) {
        PacketPlayOutPlayerListHeaderFooter headerFooter = new PacketPlayOutPlayerListHeaderFooter(header);
        try {
            if (bField == null) {
                bField = headerFooter.getClass().getDeclaredField("b");
                bField.setAccessible(true);
            }
            bField.set(headerFooter, footer);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return headerFooter;
    }
    
    public static void updateHeaderAndFooter(Player player, String header, String footer) {
        
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        IChatBaseComponent tabHeader = ChatSerializer.a("{'color': 'white', 'text': '" + Messaging.colorizeMessage(header) + "'}");
        IChatBaseComponent tabFooter = ChatSerializer.a("{'color': 'white', 'text': '" + Messaging.colorizeMessage(footer) + "'}");
        connection.sendPacket(getHeaderFooterPacket(tabHeader, tabFooter));
    }
}
