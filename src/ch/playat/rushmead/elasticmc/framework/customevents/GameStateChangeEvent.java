package ch.playat.rushmead.elasticmc.framework.customevents;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Rushmead
 */
public class GameStateChangeEvent extends Event {

    public static final HandlerList handlers = new HandlerList();
    private GameState gameState;
    private int inGameState;

    public GameStateChangeEvent(GameState gameState) {
        this.gameState = gameState;
        this.inGameState = 0;
    }

    public GameStateChangeEvent(GameState gameState, int inGameState) {
        this.gameState = gameState;
        this.inGameState = inGameState;
    }

    public GameState getGameState() {
        return gameState;
    }

    public int getInGameStateNumber() {
        return inGameState;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
