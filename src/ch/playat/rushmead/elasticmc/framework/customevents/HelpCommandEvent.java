package ch.playat.rushmead.elasticmc.framework.customevents;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rushmead
 */
public class HelpCommandEvent extends Event {

    public static final HandlerList handlers = new HandlerList();
    private List<String> messages;

    public HelpCommandEvent() {
        this.messages = new ArrayList<>();
    }

    public List<String> getCommands() {
        return messages;
    }

    public void setCommands(List<String> messages) {
        this.messages = messages;
    }

    public void addCommand(String message) {
        messages.add(message);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
