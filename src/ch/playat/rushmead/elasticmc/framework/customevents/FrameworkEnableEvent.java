

package ch.playat.rushmead.elasticmc.framework.customevents;


import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
/**
 *
 * @author Rushmead
 */
public class FrameworkEnableEvent extends Event{


      public static final HandlerList handlers = new HandlerList();

      public FrameworkEnableEvent(){

      }
      @Override
      public HandlerList getHandlers()
      {
        return handlers;
      }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

}
