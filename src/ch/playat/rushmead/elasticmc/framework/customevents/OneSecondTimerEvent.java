

package ch.playat.rushmead.elasticmc.framework.customevents;


import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
/**
 *
 * @author Rushmead
 */
public class OneSecondTimerEvent extends Event{


      public static final HandlerList handlers = new HandlerList();

      public OneSecondTimerEvent(){

      }
      @Override
      public HandlerList getHandlers()
      {
        return handlers;
      }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

}
