package ch.playat.rushmead.elasticmc.framework.api;

import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author Rushmead
 */
public class ServerSending {

    public static void sendToServer(String server) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            sendToServer(p, server);
        }
    }

    public static void sendToServer(Player player, String server) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(server);
        } catch (IOException e) {
            Messaging.sendMessage(player, "&cCould not send you to {0}", server);
        }
        player.sendPluginMessage(Framework.getInstance(), "BungeeCord", b.toByteArray());
    }
    public static void sendToServerParty(Player player, String server){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("SpecialConnect");
            out.writeUTF(server);
        } catch (IOException e) {
            Messaging.sendMessage(player, "&cCould not send you to {0}", server);
        }
        player.sendPluginMessage(Framework.getInstance(), "BungeeCord", b.toByteArray());
    }
    public static void findServer(){
        
    }
    
    public static void spinUpServer(String gamemode){
        
    }
}   
