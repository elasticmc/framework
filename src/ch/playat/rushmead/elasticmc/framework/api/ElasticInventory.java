package ch.playat.rushmead.elasticmc.framework.api;

import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public abstract class ElasticInventory {

    public abstract String getName();

    public abstract void open(Player p);

    public abstract void click(Player p, int slot);

}
