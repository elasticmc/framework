package ch.playat.rushmead.elasticmc.framework.api;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.Achievement;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class ElasticPlayer {

    private static HashMap<String, ElasticPlayer> players = new HashMap<>();

    public static HashMap<String, ElasticPlayer> getPlayers() {
        return players;
    }

    public static ElasticPlayer getPlayer(String name) {
        if (!players.containsKey(name)) {
            return null;
        }
        return players.get(name);
    }

    public static ElasticPlayer createPlayer(Player p) {
        if (!players.containsKey(p.getDisplayName())) {
            ElasticPlayer player = Players.getData().getPlayerFromUUID(p);
            if (player == null) {
                try {
                    player = Players.getData().getPlayerFromName(p);
                } catch (NullPointerException e) {

                }
            } else {
                Players.getData().updateNameFromUUID(p);
            }

            if (player == null && p != null) {
                player = new ElasticPlayer(p.getDisplayName());
            }
            players.put(p.getDisplayName(), player);
            return player;
        } else {
            return getPlayer(p.getDisplayName());
        }

    }

    public static void removePlayer(String name) {
        if (players.containsKey(name)) {
            players.remove(name);

        }
    }

    private String realName;
    private String nickName;
    private boolean frozen;

    private boolean chatMuted;
    private boolean inSpecChat;
    private String chatPrefix;
    private String chatSuffix;

    private PermissionSet permissions;
    private Rank rank;

    private long lastLogin;

    private ArrayList<String> recentMessages;
    private ArrayList<Achievement> achievements;

    public ElasticPlayer(String name) {
        this.realName = name;
        this.nickName = name;
        this.chatMuted = false;
        this.inSpecChat = false;
        this.chatPrefix = null;
        this.chatSuffix = null;
        this.permissions = PermissionSet.DEFAULT;
        this.rank = Rank.REGULAR;
        this.lastLogin = System.currentTimeMillis();
        this.recentMessages = new ArrayList<>();
        this.frozen = false;
        this.achievements = null;
    }

    public ElasticPlayer(String name, String uuid, int rank, int permissions) {
        this.realName = name;
        this.nickName = name;
        this.chatMuted = false;
        this.inSpecChat = false;
        this.chatPrefix = null;
        this.chatSuffix = null;
        this.permissions = Players.getPermissions().getSetFromPower(permissions);
        this.rank = Players.getRanks().getRankFromID(rank);
        this.lastLogin = System.currentTimeMillis();
        this.recentMessages = new ArrayList<>();
        this.frozen = false;
        this.achievements = Players.getAchievements().getAchievements(uuid);
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(realName);
    }

    public String getRealName() {
        return realName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickname(String nickName) {
        if (nickName.length() > 15) {
            this.nickName = nickName.substring(0, 15);
        } else {
            this.nickName = nickName;
        }
        if (nickName.length() == 0) {
            this.nickName = realName;
        }
    }

    public boolean isNicknamed() {
        return !getRealName().equals(getNickName());
    }

    public int getElasticBands() {
        return Players.getBands().getElasticBands(realName);
    }

    public void addElasticBands(int bands) {
        Players.getBands().setBands(realName, Players.getBands().getElasticBands(realName) + bands);
    }

    public void removeElasticBands(int bands) {
        Players.getBands().setBands(realName, Players.getBands().getElasticBands(realName) - bands);
    }

    public void setElasticBands(int bands) {
        Players.getBands().setBands(realName, bands);
    }

    public boolean hasChatMuted() {
        return chatMuted;
    }

    public void setMuted(boolean muted) {
        this.chatMuted = muted;
    }

    public boolean isInSpecChat() {
        return inSpecChat;
    }

    public void setInSpecChat(boolean specChat) {
        this.inSpecChat = specChat;
    }

    public String getChatPrefix() {
        if (chatPrefix != null) {
            return chatPrefix + " ";
        } else {
            return "";
        }
    }

    public void setChatPrefix(String prefix) {
        this.chatPrefix = prefix;
    }

    public String getChatSuffix() {
        if (chatSuffix != null) {
            return chatSuffix + " ";
        } else {
            return "";
        }
    }

    public void setChatSuffix(String chatSuffix) {
        this.chatSuffix = chatSuffix;
    }

    public PermissionSet getPermissions() {
        return permissions;
    }

    public void setPermissions(PermissionSet permissionSet) {
        this.permissions = permissionSet;
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("permissionset");
            out.writeUTF(realName);
            out.writeInt(permissionSet.getPower());
        } catch (IOException e) {

        }
        getPlayer().sendPluginMessage(Framework.getInstance(), "BungeeCord", b.toByteArray());
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("rankset");
            out.writeUTF(realName);
            out.writeInt(rank.getID());
        } catch (IOException e) {

        }
        getPlayer().sendPluginMessage(Framework.getInstance(), "BungeeCord", b.toByteArray());
    }

    public String getRankPrefix() {
        
        if (GameState.getGameState() == GameState.NOT_IN_GAME || GameState.getGameState() == GameState.LOBBY) {
           
            return getRank().getPrefix();
        }
        if(getRank().hasTwoColours()){
            return "" + getRank().getColor() + getRank().getColor2();
        }
        return "" + getRank().getColor();
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public ArrayList<String> getRecentMessages() {
        return recentMessages;
    }

    public void addRecentMessage(String message) {
        recentMessages.add(message);
        if (recentMessages.size() > 6) {
            recentMessages.remove(0);
        }
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    public ArrayList<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(ArrayList<Achievement> achievements) {
        this.achievements = achievements;
    }

    public void addAchievement(Achievement ach) {
        this.achievements.add(ach);
        if (ach.getElasticBands() > 0) {
            this.addElasticBands(ach.getElasticBands());
        }
    }
}
