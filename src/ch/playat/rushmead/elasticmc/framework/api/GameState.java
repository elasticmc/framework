/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.playat.rushmead.elasticmc.framework.api;

import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.customevents.GameStateChangeEvent;
import org.bukkit.Bukkit;

/**
 *
 * @author Stuart
 */
public enum GameState {

    LOBBY,
    SETUP,
    IN_GAME,
    ENDING,
    NOT_IN_GAME;

    private static GameState gameState;

    public static int gameTimer;

    public static GameState getGameState() {
        return gameState;

    }

    public static void setGameState(GameState state) {
        GameState.gameState = state;
        Bukkit.getPluginManager().callEvent(new GameStateChangeEvent(state));
        Framework.getInstance().startUpdateTimer();
    }
}
