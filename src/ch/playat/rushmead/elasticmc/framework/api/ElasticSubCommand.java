package ch.playat.rushmead.elasticmc.framework.api;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Rushmead
 */
public abstract class ElasticSubCommand {

    public abstract String getName();

    public abstract boolean onCommand(CommandSender sender, Command cmd, String[] args);
}
