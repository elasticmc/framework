package ch.playat.rushmead.elasticmc.framework.api;

import ch.playat.rushmead.elasticmc.framework.Framework;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Rushmead
 */
public class ElasticCommand extends Command {

    private CommandExecutor exe = null;

    public ElasticCommand(String name) {
        super(name);

    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (exe != null) {
            exe.onCommand(sender, this, commandLabel, args);

        }
        return false;
    }

    public void setExecutor(CommandExecutor exe) {
        this.exe = exe;
    }

    public static void registerCommand(String prefix, String label, CommandExecutor executor) {
        ElasticCommand cmd = new ElasticCommand(label);
        Framework.getInstance().getCommandMap().register(prefix, cmd);
        cmd.setExecutor(executor);
    }

    @Deprecated
    public static void registerCommand(String prefix, List<String> aliases, CommandExecutor executor) {
        for (String label : aliases) {
            ElasticCommand cmd = new ElasticCommand(label);
             Framework.getInstance().getCommandMap().register(prefix, cmd);
            cmd.setExecutor(executor);
        }
    }

}
