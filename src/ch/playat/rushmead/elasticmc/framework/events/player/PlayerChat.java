package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.handler.ChatHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerChat implements Listener {

    private boolean mentioned;

    @EventHandler
    public void onEvent(AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        boolean cancelled = false;
        mentioned = false;
        final Player p = e.getPlayer();

        if (Players.getPermissions().getPermissions(p.getDisplayName()).getPower() < PermissionSet.ALL.getPower()) {
            if (!cancelled) {
                cancelled = ChatHandler.hasProfanity(p, e.getMessage());
            }
            if (!cancelled && ChatHandler.isInCaps(e.getMessage())) {
                char[] msg = e.getMessage().toLowerCase().toCharArray();
                msg[0] = Character.toUpperCase(msg[0]);
                e.setMessage(new String(msg));
            }
        }

        if (!cancelled) {
            for (String msgs : e.getMessage().split(" ")) {
                if (!mentioned) {
                    if (ChatHandler.isMentioned(p, msgs)) {
                        mentioned = true;
                    }
                }
            }
        }
        if (!cancelled && Players.getPermissions().getPermissions(p.getDisplayName()).getPower() >= PermissionSet.VIB.getPower()) {
            if (e.getMessage().startsWith("!") && e.getMessage().length() > 1) {
                e.setMessage(ChatColor.BOLD + e.getMessage().replaceFirst("!", ""));
            }
        }
        if (!cancelled) {
            ChatHandler.sendMessage(p, e.getMessage());
        }
    }
}
