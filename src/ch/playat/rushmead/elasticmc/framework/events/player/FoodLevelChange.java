package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.framework.api.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 *
 * @author Rushmead
 */
public class FoodLevelChange implements Listener {

    @EventHandler
    public void onEvent(FoodLevelChangeEvent e) {
        if (GameState.getGameState() == GameState.LOBBY || GameState.getGameState() == GameState.SETUP || GameState.getGameState() == GameState.NOT_IN_GAME) {
            e.setFoodLevel(20);
        }
    }
}
