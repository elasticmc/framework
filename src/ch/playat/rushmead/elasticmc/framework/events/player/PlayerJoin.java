package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.database.players.Achievement;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import ch.playat.rushmead.elasticmc.framework.messaging.TabListManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
/**
 *
 * @author Rushmead
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onEvent(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        final Player p = e.getPlayer();
        final ElasticPlayer ep = ElasticPlayer.getPlayer(p.getName());
        Messaging.logmessage(ep.getNickName(), ep.getRank().getColor(), true);
        Servers.getPlayers().setPlayers(Bukkit.getOnlinePlayers().size());
        p.setFoodLevel(20);
        if (GameState.getGameState() != GameState.LOBBY) {
            
            return;
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Framework.getInstance(), new Runnable() {
            @Override
            public void run() {
                TabListManager.updateHeaderAndFooter(p, "&bCommunity Fun and More!", "&dplay.elasticmc.net");
                
              //  Players.getAchievements().giveAchievement(p, Achievement.freshSpawn);
                
             //   if(ep.getPermissions().getPower() >= PermissionSet.VIB.getPower()){
             //       Players.getAchievements().giveAchievement(p, Achievement.premiumElastic);
             //   }
            }

        }, 10L);
    }
}
