package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerMove implements Listener {

    @EventHandler
    public void onEvent(PlayerMoveEvent e) {
        try {
            if (ElasticPlayer.getPlayer(e.getPlayer().getDisplayName()).isFrozen()) {
                if (e.getFrom().getX() != e.getTo().getX() || e.getFrom().getZ() != e.getTo().getZ()) {
                    Location loc = e.getFrom();
                    loc.setPitch(e.getTo().getPitch());
                    loc.setYaw(e.getTo().getYaw());
                    e.getPlayer().teleport(loc);
                }
            }
        } catch (NullPointerException ex) {

        }
    }
}
