package ch.playat.rushmead.elasticmc.framework.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerCommandPreprocess implements Listener {

    @EventHandler
    public void onEvent(PlayerCommandPreprocessEvent e) {
        String msg = e.getMessage();
        Player p = e.getPlayer();
        if (msg.equals("/stop") || msg.startsWith("/stop ") || msg.equals("/restart") || msg.startsWith("/restart ")) {
            p.chat("/restartserver");
            e.setCancelled(true);
        }
    }
}
