package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

/**
 *
 * @author Rushmead
 */
public class PlayerLogin implements Listener {
    
    @EventHandler
    public void onEvent(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        ElasticPlayer ep = ElasticPlayer.createPlayer(p);
        
        if (Servers.getWhitelist().isWhitelisted() && (!Bukkit.getWhitelistedPlayers().contains(p)) && ep.getPermissions().getPower() < PermissionSet.JNR_STAFF.getPower()) {
            e.disallow(Result.KICK_WHITELIST, "&cThis server is whitelisted to ElasticMC staff members");
        }
        if (GameState.getGameState() == GameState.IN_GAME) {
            e.disallow(Result.KICK_FULL, "&cThis server is currently ingame!");
        }
        if (e.getResult() != Result.ALLOWED) {
            ElasticPlayer.removePlayer(p.getDisplayName());
        }
    }
}
