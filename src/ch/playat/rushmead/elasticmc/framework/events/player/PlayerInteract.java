package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.framework.api.ServerSending;
import ch.playat.rushmead.elasticmc.framework.items.FrameworkItems;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void onEvent(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        Action action = e.getAction();

        if (action != Action.PHYSICAL) {
            if (p.getItemInHand().equals(FrameworkItems.backToHub())) {
                ServerSending.sendToServer("hub1");
            }
        } else {
            Block clicked = e.getClickedBlock();
            if (clicked.getType() == Material.CROPS) {
                e.setCancelled(true);
            }
        }
    }
}
