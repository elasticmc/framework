package ch.playat.rushmead.elasticmc.framework.events.player;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.framework.Framework;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onEvent(PlayerQuitEvent e) {
        final Player p = e.getPlayer();
        e.setQuitMessage(null);
        if (ElasticPlayer.getPlayer(p.getDisplayName()) != null) {
            ElasticPlayer ep = ElasticPlayer.getPlayer(p.getDisplayName());
            if (ep != null) {
                Players.getAchievements().updateAchievements(p);
                Players.getData().savePlayerToDB(p);
                Messaging.logmessage(ep.getNickName(), ep.getRank().getColor(), false);
                p.getInventory().clear();
            } else {
                Messaging.logmessage(p.getDisplayName(), ChatColor.GRAY, false);
            }
        } else {
            Messaging.logmessage(p.getDisplayName(), ChatColor.GRAY, false);
        }
        if (GameState.getGameState() == GameState.LOBBY || GameState.getGameState() == GameState.IN_GAME) {
            Servers.getData().updateAllInfo();
        }

        Bukkit.getScheduler().scheduleSyncDelayedTask(Framework.getInstance(), new Runnable() {
            @Override
            public void run() {
                if(ElasticPlayer.getPlayer(p.getDisplayName()) != null){
                    ElasticPlayer.removePlayer(p.getDisplayName());
                }
            }

        }, 2L);
    }
}
