

package ch.playat.rushmead.elasticmc.framework.events.other;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
/**
 *
 * @author Rushmead
 */
public class HangingBreakByEntity implements Listener{

        @EventHandler
        public void onEvent(HangingBreakByEntityEvent e){
            e.setCancelled(true);
        }
}
