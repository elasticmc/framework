

package ch.playat.rushmead.elasticmc.framework.events.other;


import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFadeEvent;
/**
 *
 * @author Rushmead
 */
public class BlockFade implements Listener{

        @EventHandler
        public void onEvent(BlockFadeEvent e){
            Block block = e.getBlock();
            if(block.getType() == Material.ICE || block.getType() == Material.LEAVES){
                e.setCancelled(true);
            }
        }
}
