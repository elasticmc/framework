package ch.playat.rushmead.elasticmc.framework.cooldowns;

import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import java.util.HashMap;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class Cooldowns {

    private static HashMap<String, Cooldown> cooldowns = new HashMap<>();

    public static void startCooldown(String type, Player p, boolean display, int time) {
        if (!cooldowns.containsKey(p.getDisplayName() + "-" + type)) {
            cooldowns.put(p.getDisplayName() + "-" + type, new Cooldown(p.getDisplayName(), type, display, time));
        }

    }

    public static void finishCooldown(String type, Player p) {
        if (cooldowns.containsKey(p.getDisplayName() + "-" + type)) {
            cooldowns.remove(p.getDisplayName() + "-" + type);
        }
    }

    public static boolean isInCooldown(String type, Player p) {
        if (cooldowns.containsKey(p.getDisplayName() + "-" + type)) {
            if (cooldowns.get(p.getDisplayName() + "-" + type).getTimeLeft() <= 0) {
                cooldowns.remove(p.getDisplayName() + "-" + type);
            }
        }
        return cooldowns.containsKey(p.getDisplayName() + "-" + type);
    }

    public static long getTimeLeft(String type, Player p, String cooldownText) {
        if (cooldowns.containsKey(p.getDisplayName() + "-" + type)) {
            Cooldown cooldown = cooldowns.get(p.getDisplayName() + "-" + type);

            if (cooldown.hasDisplay()) {
                if (cooldown.getTimeLeft() == 1) {
                    Messaging.sendMessage(p, "&c{0} in &e{2} &csecond.", cooldownText, cooldown.getTimeLeft());
                } else {
                    Messaging.sendMessage(p, "&c{0} in &e{2} &cseconds", cooldownText, cooldown.getTimeLeft());
                }
            }
            return cooldown.getTimeLeft();
        }
        return 0;
    }
}
