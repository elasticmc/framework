

package ch.playat.rushmead.elasticmc.framework.cooldowns;

import java.util.Date;

/**
 *
 * @author Rushmead
 */
public class Cooldown {

    
    private String playerName;
    private String cooldownType;
    private boolean display;
    private long startTime;
    private int duration;
    
    
    public Cooldown(String playerName, String cooldownType, boolean display, int duration){
        this.playerName = playerName;
        this.cooldownType = cooldownType;
        this.display = display;
        this.startTime = new Date().getTime();
        this.duration = duration;
    }
    
    public String getPlayerName(){
        return playerName;
    }
    
    public String getType(){
        return cooldownType;
    }
    
    public boolean hasDisplay(){
        return display;
    }
    
    public long getStartTime(){
        return startTime;
    }
    public long getLength(){
        return duration;
    }
    public long getTimeLeft(){
        return ((getLength() * 1000 - new Date().getTime()) + getStartTime()) / 1000;
    }
}
