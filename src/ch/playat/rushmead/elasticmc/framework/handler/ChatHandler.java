package ch.playat.rushmead.elasticmc.framework.handler;

import ch.playat.rushmead.elasticmc.database.Players;
import ch.playat.rushmead.elasticmc.database.players.PermissionSet;
import ch.playat.rushmead.elasticmc.database.players.Rank;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.cooldowns.Cooldowns;
import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rushmead
 */
public class ChatHandler {

    private static boolean muted;
    public static List<String> bannedWords = new ArrayList<>();

    public static boolean isMuted() {
        return muted;
    }

    public static void setMuted(boolean chatMuted) {
        muted = chatMuted;
    }

    public static void sendMessage(Player p, String message) {
        ElasticPlayer ep = ElasticPlayer.getPlayer(p.getDisplayName());

        ChatColor color = ChatColor.WHITE;

        if (ep.getRank() == Rank.REGULAR) {
            color = ChatColor.GRAY;
        }

        String coloredMessage = Messaging.colorizeMessage(ep.getChatPrefix() + ep.getRankPrefix() + ep.getNickName() + "&8: " + color) + message + " " + Messaging.colorizeMessage(ep.getChatSuffix());
        
        if(GameState.getGameState() == GameState.IN_GAME && ep.isInSpecChat()){
            for(Player ps : Bukkit.getOnlinePlayers()){
                ElasticPlayer eps = ElasticPlayer.getPlayer(ps.getDisplayName());
                if(eps.isInSpecChat()){
                    ps.sendMessage(coloredMessage);
                }
            }
        }else {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                try {
                    if (ElasticPlayer.getPlayer(ps.getName()).hasChatMuted()) {
                        return;
                    }
                }
                catch (NullPointerException ex) {
                }
                ps.sendMessage(coloredMessage);
            }
        }

        Bukkit.getConsoleSender().sendMessage(coloredMessage);
        ep.addRecentMessage(coloredMessage);
    }
    
    public static String getMessage(Player p, ChatColor color, String message){
        ElasticPlayer ep = ElasticPlayer.getPlayer(p.getDisplayName());
        if(Players.getPermissions().getPermissions(p.getDisplayName()).getPower() >= PermissionSet.JNR_STAFF.getPower() && ep.isNicknamed()){
            return Messaging.colorizeMessage(ep.getChatPrefix() + ep.getRankPrefix() + ep.getNickName() + " &7(" + ep.getRealName() + ")&8: " + color) + message + " " + Messaging.colorizeMessage(ep.getChatSuffix());
        }else{
              return Messaging.colorizeMessage(ep.getChatPrefix() + ep.getRankPrefix() + ep.getNickName() + "&8: " + color) + message + " " + Messaging.colorizeMessage(ep.getChatSuffix());
        }
    }
    
    public static boolean isInCaps(String message){
        boolean isInCaps = false;
        
        if(message.length() > 4){
            String msg = message.replaceAll("/[^A-Za-z0-9 ]/", "");
            int caps = 0;
            for(char c : msg.toCharArray()){
                if(Character.isUpperCase(c)){
                    caps++;
                }
            }
            isInCaps = msg.length() / 2 < caps;
        }
        return isInCaps;
    }
    
    public static boolean hasProfanity(Player p, String message){
        if(Players.getPermissions().getPermissions(p.getDisplayName()) == PermissionSet.ALL){
            return false;
        }
        
        for(String msgs : message.split(" ")){
            if(bannedWords.contains(msgs.toLowerCase())){
                Messaging.sendMessage(p, "&cWe don't allow the use of profanity.");
                return true;
            }
        }
        return false;
    }
    
    public static boolean isSpamming(ElasticPlayer p, String message){
        boolean isSpamming = false;
        HashMap<Character, Integer> letters = new HashMap<>();
        for(char c : message.toCharArray()){
            if(letters.containsKey(c)){
                int i = letters.get(c);
                i++;
                letters.remove(c);
                letters.put(c, i);
            }else{
                letters.put(c, 1);
            }
        }
        for(int i : letters.values()){
            if( i > (message.length() / 4) * 3 && message.length() > 6){
                isSpamming = true;
            }
        }
        return isSpamming;
    }
    public static boolean isMentioned(Player p, String message){
        boolean isMentioned = false;
        
        if(message.startsWith("@") && message.length() > 0){
            if(Players.getPermissions().getPermissions(p.getDisplayName()).getPower() == PermissionSet.DEFAULT.getPower()){
                Messaging.sendMessage(p, "&cPinging is a VIB and Up Feature.");
                return false;
            }
            
            if(Cooldowns.isInCooldown("playerping", p)){
                Cooldowns.getTimeLeft("playerping", p, "&cMention not sent. You can metnion a user again.");
                
            }else{
                try{
                    Player pingP = Bukkit.getServer().getPlayer(message.substring(1));
                    if(!pingP.isOnline()){
                        return false;
                    }
                    if(p == pingP){
                        Messaging.sendMessage(p, "&cIt looks like your trying to ping yourself. Get some friends.");
                        return false;
                    }
                     if (Players.getRanks().getRankFromDatabase(pingP.getDisplayName()).hasTwoColours()) {
                   Messaging.sendMessage(p, "&cYou just pinged {0}", Players.getRanks().getRankFromDatabase(pingP.getDisplayName()).getColor2() + "" + Players.getRanks().getRankFromDatabase(pingP.getDisplayName()).getColor() + pingP.getDisplayName());
                } else {

                   Messaging.sendMessage(p, "&cYou just pinged {0}", Players.getRanks().getRankFromDatabase(pingP.getDisplayName()).getColor() + pingP.getDisplayName());
                }
                     if(Players.getRanks().getRankFromDatabase(p.getDisplayName()).hasTwoColours()){
                         Messaging.sendMessage(pingP, "&c\u273A &6You've been &c&lPINGED &r&6by {0}! &c\u273A", Players.getRanks().getRankFromDatabase(p.getDisplayName()).getColor2() + "" + Players.getRanks().getRankFromDatabase(p.getDisplayName()).getColor()+ p.getDisplayName());
                     }else{
                         Messaging.sendMessage(pingP, "&c\u273A &6You've been &c&lPINGED &r&6by {0}! &c\u273A", Players.getRanks().getRankFromDatabase(p.getDisplayName()).getColor() + p.getDisplayName());
                     }
                    
                    pingP.playSound(pingP.getLocation(), Sound.LEVEL_UP, 1, 1);
                    
                    if(Players.getPermissions().getPermissions(p.getDisplayName()).getPower() <= PermissionSet.JNR_STAFF.getPower()){
                        Cooldowns.startCooldown("playerping", p, true, 10);
                    }
                    return true;
                }catch(NullPointerException e){
                    return false;
                }
            }
        }
        return isMentioned;
    }
}
