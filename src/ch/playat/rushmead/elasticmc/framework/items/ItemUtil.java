package ch.playat.rushmead.elasticmc.framework.items;

import ch.playat.rushmead.elasticmc.framework.messaging.Messaging;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bukkit.enchantments.Enchantment;

/**
 *
 * @author Rushmead
 */
public class ItemUtil {

    public static ItemStack createItem(Material material) {
        return new ItemStack(material);
    }

    public static ItemStack createItem(Material material, byte data) {
        ItemStack item = new ItemStack(material);
        item.setData(new MaterialData(data));
        return item;
    }

    public static ItemStack createItem(Material material, String name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItemSpecial(Material material, byte data, String name) {
        ItemStack item = new ItemStack(material, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, byte data, String name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        item.setData(new MaterialData(data));
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createItem(Material material, byte data, String name, String... lore) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        item.setData(new MaterialData(data));

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount) {
        return new ItemStack(material, amount);
    }

    public static ItemStack createStack(Material material, int amount, byte data) {
        ItemStack item = new ItemStack(material, amount);
        item.setData(new MaterialData(data));
        return item;
    }

    public static ItemStack createStack(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount, byte data, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        item.setData(new MaterialData(data));
        meta.setDisplayName(Messaging.colorizeMessage(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount, String name, String... lore) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createStack(Material material, int amount, byte data, String name, String... lore) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();
        item.setData(new MaterialData(data));

        List<String> lores = new ArrayList<>();
        for (String aLore : lore) {
            lores.add(Messaging.colorizeMessage(aLore));
        }
        meta.setDisplayName(Messaging.colorizeMessage(name));
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack getPotion(PotionType type, boolean splash) {
        Potion pot = new Potion(type);
        pot.setSplash(splash);
        return pot.toItemStack(1);
    }

    public static String toString(ItemStack i) {
        return mapToString(i.serialize());
    }

    public static ItemStack fromString(String s) {
        return ItemStack.deserialize(stringToMap(s));
    }

    private static String mapToString(Map<String, Object> map) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String key : map.keySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append("&");
            }
            String value = map.get(key).toString();
            try {
                stringBuilder.append((key != null ? URLEncoder.encode(key, "UTF-8") : ""));
                stringBuilder.append("=");
                stringBuilder.append(value != null ? URLEncoder.encode(value, "UTF-8") : "");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }
        return stringBuilder.toString();
    }

    public static boolean isParsable(String input) {
        boolean parsable = true;
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException e) {
            parsable = false;
        }
        return parsable;
    }

    private static Map<String, Object> stringToMap(String input) {
        Map<String, Object> map = new HashMap<String, Object>();
        String[] nameValuePairs = input.split("&");
        for (String nameValuePair : nameValuePairs) {
            String[] nameValue = nameValuePair.split("=");

            try {

                if (isParsable(nameValue[1])) {
                    map.put(URLDecoder.decode(nameValue[0], "UTF-8"),
                            (int) Integer.parseInt(nameValue[1]));
                } else {
                    map.put(URLDecoder.decode(nameValue[0], "UTF-8"),
                            nameValue.length > 1 ? URLDecoder.decode(nameValue[1], "UTF-8") : "");
                }
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("This method requires UTF-8 encoding support", e);
            }
        }

        return map;
    }

}
