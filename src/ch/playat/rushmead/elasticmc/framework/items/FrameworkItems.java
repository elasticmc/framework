package ch.playat.rushmead.elasticmc.framework.items;


import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Rushmead
 */
public class FrameworkItems {

    public static ItemStack backToHub() {
        return ItemUtil.createItem(Material.EYE_OF_ENDER, "&bBack to Hub &7&o(Right Click)");
    }

    /**
     * Get the enchanted itemstack.
     *
     * @param type The item type
     * @param enchant The enchantment
     * @param level The enchant level
     *
     * @return The enchanted itemstack
     */
    public static ItemStack getEnchantedItem(Material type, Enchantment enchant, int level) {
        ItemStack item = new ItemStack(type);
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(enchant, level, true);
        item.setItemMeta(meta);
        return item;
    }

}
