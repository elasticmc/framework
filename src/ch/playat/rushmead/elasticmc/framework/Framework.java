package ch.playat.rushmead.elasticmc.framework;

import ch.playat.rushmead.elasticmc.database.DatabaseConnections;
import ch.playat.rushmead.elasticmc.database.Servers;
import ch.playat.rushmead.elasticmc.framework.api.ElasticCommand;
import ch.playat.rushmead.elasticmc.framework.api.ElasticPlayer;
import ch.playat.rushmead.elasticmc.framework.api.GameState;
import ch.playat.rushmead.elasticmc.framework.commands.admincommands.AddBandsCommand;
import ch.playat.rushmead.elasticmc.framework.commands.admincommands.GMCommand;
import ch.playat.rushmead.elasticmc.framework.commands.admincommands.RestartCommand;
import ch.playat.rushmead.elasticmc.framework.commands.admincommands.SetRankCommand;
import ch.playat.rushmead.elasticmc.framework.commands.modcommands.KickCommand;
import ch.playat.rushmead.elasticmc.framework.commands.usercommands.HelpCommand;
import ch.playat.rushmead.elasticmc.framework.commands.usercommands.ListCommand;
import ch.playat.rushmead.elasticmc.framework.commands.usercommands.ListRanksCommand;
import ch.playat.rushmead.elasticmc.framework.customevents.FrameworkEnableEvent;
import ch.playat.rushmead.elasticmc.framework.customevents.OneMinuteTimerEvent;
import ch.playat.rushmead.elasticmc.framework.customevents.OneSecondTimerEvent;
import ch.playat.rushmead.elasticmc.framework.events.other.BlockFade;
import ch.playat.rushmead.elasticmc.framework.events.other.HangingBreakByEntity;
import ch.playat.rushmead.elasticmc.framework.events.other.LeavesDecay;
import ch.playat.rushmead.elasticmc.framework.events.other.WeatherChange;
import ch.playat.rushmead.elasticmc.framework.events.player.FoodLevelChange;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerChat;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerCommandPreprocess;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerInteract;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerJoin;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerLogin;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerMove;
import ch.playat.rushmead.elasticmc.framework.events.player.PlayerQuit;
import ch.playat.rushmead.elasticmc.framework.handler.ChatHandler;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.lang.reflect.Field;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Rushmead
 */
public class Framework extends JavaPlugin {

    private static Framework instance;
    private CommandMap commandMap;
    private int serverUpdateTimer;

    public static Framework getInstance() {
        return instance;
    }

    public CommandMap getCommandMap() {
        return commandMap;
    }

    @Override
    public void onEnable() {
        instance = this;
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        DatabaseConnections.openConnection();
        if (!new File(getDataFolder(), "config").isFile()) {
            File configFile = new File(getDataFolder(), "config.yml");
            configFile.delete();
            saveDefaultConfig();
            getConfig().options().copyDefaults(true);
        }
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new BlockFade(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new HangingBreakByEntity(), this);
        pm.registerEvents(new LeavesDecay(), this);
        pm.registerEvents(new PlayerChat(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerLogin(), this);
        pm.registerEvents(new PlayerMove(), this);
        pm.registerEvents(new PlayerQuit(), this);
        pm.registerEvents(new WeatherChange(), this);
        pm.registerEvents(new PlayerCommandPreprocess(), this);
        saveDefaultConfig();
        ChatHandler.setMuted(false);
       
        if (getServer() instanceof CraftServer) {
            Field f;
            try {
                f = CraftServer.class.getDeclaredField("commandMap");
                f.setAccessible(true);
                commandMap = (CommandMap) f.get(getServer());
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | NullPointerException | SecurityException ex) {
                ex.printStackTrace();
            }
        }

        ElasticCommand.registerCommand("framework", "addbands", new AddBandsCommand());
        ElasticCommand.registerCommand("framework", "gm", new GMCommand());
        ElasticCommand.registerCommand("framework", "gamemode", new GMCommand());
        ElasticCommand.registerCommand("framework", "help", new HelpCommand());
        ElasticCommand.registerCommand("framework", "kick", new KickCommand());
        ElasticCommand.registerCommand("framework", "list", new ListCommand());
        ElasticCommand.registerCommand("framework", "listranks", new ListRanksCommand());
        ElasticCommand.registerCommand("framework", "restartserver", new RestartCommand());
        ElasticCommand.registerCommand("framework", "restart", new RestartCommand());
        ElasticCommand.registerCommand("framework", "setrank", new SetRankCommand());

        for (String s : getConfig().getStringList("bannedwords")) {
            ChatHandler.bannedWords.add(s);
        }
        for (Player ps : Bukkit.getOnlinePlayers()) {
            ElasticPlayer.createPlayer(ps);
        }
        GameState.setGameState(Servers.getState().getStateFromNumber(Servers.getState().getGameState(Servers.getData().getName())));
        Servers.getData().setName(Servers.getData().getNameFromDB());
System.out.print("I am Server Nicknamed:"+ Servers.getData().getName());
System.out.print("My GameState:"+ GameState.getGameState());
         Bukkit.getScheduler().scheduleAsyncDelayedTask(
                this,
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        FrameworkEnableEvent enableEvent = new FrameworkEnableEvent();
                        Bukkit.getServer().getPluginManager().callEvent(enableEvent);
                        Servers.getWhitelist().isWhitelistedFromDB();
                    }

                }, 2L);

        Bukkit.getScheduler()
                .scheduleSyncRepeatingTask(
                        this,
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                OneSecondTimerEvent e = new OneSecondTimerEvent();
                                Bukkit.getServer().getPluginManager().callEvent(e);
                            }

                        }, 20L, 20L);

        Bukkit.getScheduler()
                .scheduleSyncRepeatingTask(
                        this,
                        new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                System.gc();
                                OneMinuteTimerEvent e = new OneMinuteTimerEvent();
                                Bukkit.getServer().getPluginManager().callEvent(e);
                            }

                        }, 20L, 60 * 20L);
    }
    
      public void startUpdateTimer()
    {
        int updateTime = 10;
        GameState state = GameState.getGameState();

        if (state == GameState.LOBBY) {
            updateTime = 3;
        }
        if (state == GameState.IN_GAME) {
            updateTime = 20;
        }
        if (state == GameState.NOT_IN_GAME) {
            updateTime = 30;
        }

        Bukkit.getScheduler().cancelTask(serverUpdateTimer);
        serverUpdateTimer = getServer().getScheduler().scheduleSyncRepeatingTask(
                this,
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Servers.getData().updateAllInfo();
                    }

                }, 40L, updateTime * 20L);
    }

}
